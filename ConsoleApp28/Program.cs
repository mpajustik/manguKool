﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp28
{
    enum Sugu { Naine, Mees }
    class Program
    {

        //Olen valmis saanud inimeste genereerimise.
        static void Main(string[] args)
        {
            Inimene isik = new Inimene();
            List<string> genereeritudIsikuList = new List<string>();
            Isikukood uus = new Isikukood();
            DateTime kontrollAeg = new DateTime();

            //for (int i = 0; i < genereeritudIsikuList.Count; i++)
            //{
            //    Console.WriteLine($"{i + 1} isiku nimi on {genereeritudIsikuList[i]}");
            //}

            //for (int i = 0; i < 10; i++)
            //{
            //    genereeritudIsikuList.Add($"{isik.LooInimene(2)}");
            //}
            //foreach (var isikud in genereeritudIsikuList)
            //{
            //    Console.WriteLine(isikud);
            //}
            //Console.WriteLine($"Loodi list, kus asub {genereeritudIsikuList.Count} uut inimest");

            

            

            for (int i = 0; i < 50; i++)
            {
                Console.Write(uus.SuvalineIsikukood());
                string kogunumber = null;
                Console.Write($"\t{uus.ToSugu(kogunumber)}\t");
                string Aastaks;
                Aastaks = uus.Aastad.ToString();
                if (uus.Aastad <= 99 && uus.Aastad >= 20)
                {
                    kontrollAeg = DateTime.Parse($"{uus.Päevad},{uus.Kuud},{uus.Aastad}");
                    Console.Write(kontrollAeg.ToShortDateString());
                }
                else
                {
                    
                    kontrollAeg = DateTime.Parse($"{uus.Päevad},{uus.Kuud},{uus.Aastad}");
                    Console.Write(kontrollAeg.ToShortDateString());
                }

                Console.WriteLine($"\t{isik.LooInimene(uus.Sajand)}");

            }
        }
    }

    class Inimene
    {
        string _EesNimi;
        string _PerekonnaNimi;
        Random r = new Random();

        //2014 aasta kõige populaarsemad poislapse nimed
        string[] EesNimedPoisid = new string[] {
            "Rasmus",
            "Artjom",
            "Robin",
            "Martin",
            "Oliver",
            "Romet",
            "Sebastian",
            "Robert",
            "Artur",
            "Maksim",
            "Markus",
            "Marten",
            "Karl",
            "Kristofer",
            "Oskar",
            "Daniel",
            "Hugo",
            "Henri",
            "Mark",
            "Nikita",
            "Kirill",
            "Sander",
            "Kevin",
            "Aleksandr",
            "Daniil"  };
        //2014 aasta kõige populaarsemad tütarlapse nimed
        string[] EesNimedTüdrukud = new string[]
        {
            "Sofia",
            "Eliise",
            "Maria",
            "Mia",
            "Lisandra",
            "Mirtel",
            "Sandra",
            "Emma",
            "Laura",
            "Darja",
            "Arina",
            "Milana",
            "Alisa",
            "Anastasia",
            "Lenna",
            "Liisa",
            "Anna",
            "Viktoria",
            "Elisabeth",
            "Polina",
            "Marta",
            "Aleksandra",
            "Marleen",
            "Hanna",
            "Nora"
        };

        string[] PerekonnaNimed = new string[]
        {

            "Ivanov",
            "Tamm",
            "Saar",
            "Sepp",
            "Mägi",
            "Smirnov",
            "Vasiliev",
            "Petrov",
            "Kask",
            "Kukk",
            "Kuznetsov",
            "Rebane",
            "Ilves",
            "Mihhailov",
            "Pärn",
            "Pavlov",
            "Semenov",
            "Koppel",
            "Andreev",
            "Alekseev",
            "Luik",
            "Kaasik",
            "Lepik",
            "Oja",
            "Raudsepp",
            "Kuusk",
            "Karu",
            "Fjodorov",
            "Nikolaev",
            "Kütt",
            "Põder",
            "Vaher",
            "Popov",
            "Stepanov",
            "Volkov",
            "Moroz",
            "Lepp",
            "Koval",
            "Kivi",
            "Kallas",
            "Kozlov",
            "Mets",
            "Sokolov",
            "Liiv",
            "Grigorieva",
            "Jakovlev",
            "Kuusik",
            "Teder",
            "Lõhmus",
            "Laur",
        };
        public Inimene()
        {

        }

        public string EesNimi { get => _EesNimi; set => _EesNimi = value; }
        public string PerekonnaNimi { get => _PerekonnaNimi; set => _PerekonnaNimi = value; }

        public string LooInimene(int Sajand)
        {
            //int suvaline = r.Next(0, 2);
            string genereeriEesnimi;
            string genereeriPerekonnanimi;
            switch (Sajand)
            {
                case 3:
                case 5:
                    int uusNumberPoisid = r.Next(0, EesNimedPoisid.Length);
                    genereeriEesnimi = EesNimedPoisid[uusNumberPoisid];
                    break;

                case 4:
                case 6:
                    int uusNumberTüdrukud = r.Next(0, EesNimedTüdrukud.Length);
                    genereeriEesnimi = EesNimedTüdrukud[uusNumberTüdrukud];
                    break;
                default:
                    genereeriEesnimi = "Jaan";
                    break;
            }

            int uusNumberPerekonnaNimi = r.Next(0, PerekonnaNimed.Length);
            genereeriPerekonnanimi = PerekonnaNimed[uusNumberPerekonnaNimi];

            return $"{genereeriEesnimi} {genereeriPerekonnanimi}";
        }
    }

    class Isikukood
    {
        int _Sajand;
        int _Aastad;
        int _Kuud;
        int _Päevad;
        int _NeliViimast;
        string _KoguNumber;

        public static Random r = new Random();
        public Isikukood()
        {

        }

        //r.Next(5, 10));  luuakse int vahemikus 5 kuni 9

        public string SuvalineIsikukood()
        {
            Sajand = r.Next(3, 7);
            if (Sajand == 3 || Sajand == 4)
            {
                Aastad = (r.Next(40, 100) + 1);
                if (Aastad == 100)
                {
                    Aastad = 00;
                }
            }
            else
            {
                Aastad = r.Next(01, 18);
            }
            Kuud = r.Next(1, 13);
            switch (Kuud)
            {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:

                    Päevad = r.Next(1, 32);

                    break;
                case 2:
                    Päevad = r.Next(1, 29);
                    break;
                case 4:
                case 6:
                case 9:
                case 11:
                    Päevad = r.Next(1, 31);
                    break;

                default:
                    Päevad = r.Next(1, 29);
                    break;
            }

            NeliViimast = r.Next(1000, 10000);
            if (Aastad < 10 && Kuud < 10 && Päevad < 10)
            {
                return KoguNumber = ($"{Sajand}0{Aastad}0{Kuud}0{Päevad}{NeliViimast}");
            }
            else if (Aastad < 10 && Kuud < 10)
            {
                return KoguNumber = ($"{Sajand}0{Aastad}0{Kuud}{Päevad}{NeliViimast}");
            }
            else if (Aastad < 10 && Päevad < 10)
            {
                return KoguNumber = ($"{Sajand}0{Aastad}{Kuud}0{Päevad}{NeliViimast}");
            }
            else if (Päevad < 10 && Kuud < 10)
            {
                return KoguNumber = ($"{Sajand}{Aastad}0{Kuud}0{Päevad}{NeliViimast}");
            }
            else if (Aastad < 10)
            {
                return KoguNumber = ($"{Sajand}{Aastad}0{Kuud}{Päevad}{NeliViimast}");
            }
            else if (Kuud < 10)
            {
                return KoguNumber = ($"{Sajand}{Aastad}0{Kuud}{Päevad}{NeliViimast}");
            }
            else if (Päevad < 10)
            {
                return KoguNumber = ($"{Sajand}{Aastad}{Kuud}0{Päevad}{NeliViimast}");
            }
            return KoguNumber = ($"{Sajand}{Aastad}{Kuud}{Päevad}{NeliViimast}");



        }
        public int Sajand
        {
            get => _Sajand;
            set => _Sajand = value;
        }
        public int Aastad
        {
            get => _Aastad;
            set => _Aastad = value;
        }
        public int Kuud
        {
            get => _Kuud;
            set => _Kuud = value;
        }
        public int Päevad
        {
            get => _Päevad;
            set => _Päevad = value;
        }
        public int NeliViimast
        {
            get => _NeliViimast;
            set => _NeliViimast = value;
        }
        public string KoguNumber { get => _KoguNumber; set => _KoguNumber = value; }

        public Sugu ToSugu(string kogunumber)
        {
            return (KoguNumber[0] - '0') % 2 == 0 ? Sugu.Naine : Sugu.Mees;
        }

    }
}
